#!/bin/bash
set -ex

SSH_DIR=$HOME/.ssh

echo "remove the pair session"
mv $SSH_DIR/authorized_keys.bak $SSH_DIR/authorized_keys
tmux kill-session -t pair
