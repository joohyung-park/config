#!/bin/bash
set -ex

SSH_DIR=$HOME/.ssh

echo "new pair session"
mv $SSH_DIR/authorized_keys $SSH_DIR/authorized_keys.bak
cp $SSH_DIR/authorized_keys.bak $SSH_DIR/authorized_keys
for filename in "$@"
do
    if [ -f $SSH_DIR/$filename.pub ]
    then
	echo -ne "\ncommand=\"tmux attach -t pair\",no-port-forwarding,no-x11-forwarding,no-agent-forwarding " >> $SSH_DIR/authorized_keys
	cat $SSH_DIR/$filename.pub >> $SSH_DIR/authorized_keys
    fi
done
tmux new -s pair
